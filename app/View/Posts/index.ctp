<?php ?>
<div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
    <?php foreach ($posts as $post): ?>

        <div class="post-preview">
            <h2 class="post-title">
              <?php echo $this->Html->link($post['Post']['title'],
                    array('controller' => 'posts', 'action' => 'view', $post['Post']['id'])); ?>
            </h2>
          <p class="post-meta">Posted by
            <a href="#"></a>
                <td><?php echo $post['Post']['created']; ?></td>
            </p>
        </div>
        <hr>
    <?php endforeach; ?>
    <?php unset($post); ?>
  
      </div>
    </div>